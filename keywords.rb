#gettoken.rb

#!/usr/bin/env ruby

#require 'bundler/setup'
require 'highline/import'

File.delete(".keywords") if File.exists?(".keywords")

print "how many key words? " 
$keywords_number = Integer(gets.chomp)

$i = 0
begin
  key_word = ask("key word? #{$i+1} of #{$keywords_number}")
  File.open(".keywords", "a+") do |f|
    f.write "KEYWORD_#{$i} = '#{key_word}'\n"
  end
  $i +=1
end while $i < $keywords_number


